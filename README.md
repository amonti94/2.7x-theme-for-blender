# 2.7x theme for Blender 2.8

The original 2.5 - 2.79 theme ported and adapted to the Blender 2.8x series.

![](images/default.png)


